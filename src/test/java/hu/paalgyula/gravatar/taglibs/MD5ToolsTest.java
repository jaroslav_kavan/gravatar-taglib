package hu.paalgyula.gravatar.taglibs;

import org.junit.Test;

import org.apache.log4j.Logger;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: paal.gyula
 * Date: 2012.11.21.
 * Time: 10:27
 */
public class MD5ToolsTest {
    private static Logger logger = Logger.getLogger(MD5Tools.class);

    @Test
    public void testMd5Hex() throws Exception {
        String testEmail = "paalgyula@paalgyula.hu";
        logger.info("Md5 for " + testEmail + " - " + MD5Tools.md5Hex( testEmail ));
        assertEquals(MD5Tools.md5Hex( testEmail ), "fee33a99db5ec9375e0aeb3accf66168");
    }
}
