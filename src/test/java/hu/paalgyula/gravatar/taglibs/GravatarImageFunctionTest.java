package hu.paalgyula.gravatar.taglibs;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.11.21.
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */
public class GravatarImageFunctionTest {
    Logger logger = Logger.getLogger( GravatarImageFunction.class );

    @Test
    public void testImage() throws Exception {
        String testEmail = "paalgyula@paalgyula.hu";

        String gravatarUrl = GravatarImageFunction.image( testEmail, 32, false );

        logger.info( String.format( "Gravatar url for email %s - %s", testEmail, gravatarUrl ) );
    }
}
