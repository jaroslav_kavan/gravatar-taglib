package hu.paalgyula.gravatar.taglibs;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.11.20.
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public class MD5Tools {
    private static final Logger logger = Logger.getLogger(MD5Tools.class);

    public static String md5Hex(String message) {
        try {
            byte[] hash = MessageDigest.getInstance("MD5").digest(message.getBytes());
            BigInteger bi = new BigInteger(1, hash);
            String result = bi.toString(16);
            if (result.length() % 2 != 0) {
                return "0" + result;
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            logger.fatal(e);
        }
        return null;
    }
}
