package hu.paalgyula.gravatar.taglibs;

import static hu.paalgyula.gravatar.taglibs.MD5Tools.md5Hex;

/**
 * Created with IntelliJ IDEA.
 * User: paal.gyula
 * Date: 2012.11.20.
 * Time: 14:48
 */
public class GravatarImageFunction {

    /**
     * Generate a link for gravatar
     * @param email - image owner email
     * @param size - image size
     * @param useHttps - plain or https protocol
     * @return
     */
    public static String image( String email, int size, boolean useHttps ) {
        String md5hash = md5Hex( email.trim().toLowerCase() );

        String baseURL = "http://www.gravatar.com";
        if ( useHttps )
            baseURL = "https://secure.gravatar.com";

        return String.format("%s/avatar/%s?size=%d", baseURL, md5hash, size);
    }
}
