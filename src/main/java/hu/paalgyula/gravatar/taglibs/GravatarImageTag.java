package hu.paalgyula.gravatar.taglibs;

import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import static hu.paalgyula.gravatar.taglibs.MD5Tools.md5Hex;

/**
 * Created with IntelliJ IDEA.
 * User: paal.gyula
 * Date: 2012.11.19.
 * Time: 20:03
 */
public class GravatarImageTag extends TagSupport {
    private String email;
    private int size = 32;
    private boolean forceHttps = false;

    private Logger logger = Logger.getLogger( getClass() );

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setForceHttps( boolean forceHttps ) {
        this.forceHttps = forceHttps;
    }

    @Override
    public int doStartTag() throws JspException {
        logger.debug( "Creating gravatar link for email: " + email );
        String md5hash = md5Hex( email.trim().toLowerCase() );

        String baseURL = "http://www.gravatar.com";

        if ( forceHttps || pageContext.getRequest().getProtocol().equals( "https" ) )
            baseURL = "https://secure.gravatar.com";

        try {
            String link = String.format("%s/avatar/%s?size=%d", baseURL, md5hash, size);
            logger.debug( "Link to gravatar: " + link );
            pageContext.getOut().write( link );
        } catch (IOException e) {
            throw new JspException( e );
        }

        return SKIP_BODY;
    }
}
